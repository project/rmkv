<?php

namespace Drupal\rmkv\Commands;

use Drupal\Core\Extension\ModuleHandler;
use Drupal\Core\Extension\ProfileExtensionList;
use Drupal\Core\Extension\ThemeHandler;
use Drupal\Core\KeyValueStore\KeyValueFactoryInterface;
use Drush\Commands\DrushCommands;
use Drush\Log\LogLevel;

/**
 * Implements form of remove key/value.
 *
 * Remove system.schema key/value storage.
 */
class RemoveKeyValueCommands extends DrushCommands {

  /**
   * System schema of Key/value storage.
   *
   * @var \Drupal\Core\KeyValueStore\DatabaseStorage
   */
  protected $systemSchema;

  /**
   * Profile extension list.
   *
   * @var \Drupal\Core\Extension\ProfileExtensionList
   */
  protected $profileExtensionList;

  /**
   * Module handler.
   *
   * @var \Drupal\Core\Extension\ModuleHandler
   */
  protected $moduleHandler;

  /**
   * Module handler.
   *
   * @var \Drupal\Core\Extension\ThemeHandler
   */
  protected $themeHandler;

  /**
   * Constructs command of remove key/value.
   *
   * @param \Drupal\Core\KeyValueStore\KeyValueFactoryInterface $key_val
   *   The key/value storage collection.
   * @param \Drupal\Core\Extension\ProfileExtensionList $profile_extension_list
   *   The profile extension list.
   * @param \Drupal\Core\Extension\ModuleHandler $module_handler
   *   The module handler.
   * @param \Drupal\Core\Extension\ThemeHandler $theme_handler
   *   The theme handler.
   */
  public function __construct(KeyValueFactoryInterface $key_val, ProfileExtensionList $profile_extension_list, ModuleHandler $module_handler, ThemeHandler $theme_handler) {
    $this->systemSchema = $key_val->get('system.schema');
    $this->profileExtensionList = $profile_extension_list;
    $this->moduleHandler = $module_handler;
    $this->themeHandler = $theme_handler;
  }

  /**
   * Check if be removable of the specified machine name from key/value storage.
   *
   * @param string $machine_name
   *   The given machine name.
   *
   * @command rmkv:check
   * @usage drush rmkv:check <machine_name>
   */
  public function rmkvCheck($machine_name) {
    if ($this->systemSchema->has($machine_name) && !$this->profileExtensionList->exists($machine_name) && !$this->moduleHandler->moduleExists($machine_name) && !$this->themeHandler->themeExists($machine_name)) {
      $this->logger()->log(LogLevel::OK, \dt('Specified machine name "@machine_name" is removable from the system.schema key/value storage.', [
        '@machine_name' => $machine_name,
      ]));
    }
    else {
      $this->logger()->log(LogLevel::WARNING, \dt('Specified machine name "@machine_name" is not removable from the system.schema key/value storage.', [
        '@machine_name' => $machine_name,
      ]));
    }
  }

  /**
   * Remove specified machine name from key/value storage.
   *
   * @param string $machine_name
   *   The given machine name.
   *
   * @command rmkv
   * @usage drush rmkv <machine_name>
   */
  public function rmkv($machine_name) {
    if (!$this->systemSchema->has($machine_name)) {
      $this->logger()->log(LogLevel::ERROR, \dt('Specified machine name "@machine_name" is not exists to the system.schema key/value storage.', [
        '@machine_name' => $machine_name,
      ]));
    }
    elseif ($this->profileExtensionList->exists($machine_name) || $this->moduleHandler->moduleExists($machine_name) || $this->themeHandler->themeExists($machine_name)) {
      $this->logger()->log(LogLevel::ERROR, \dt('Cannot specify the machine name "@machine_name" of the installed profile, module or theme. Specify the machine name of the uninstalled profile, module or theme.', [
        '@machine_name' => $machine_name,
      ]));
    }
    elseif ($this->systemSchema->has($machine_name)) {
      if (!$this->profileExtensionList->exists($machine_name) && !$this->moduleHandler->moduleExists($machine_name) && !$this->themeHandler->themeExists($machine_name)) {
        $this->systemSchema->delete($machine_name);
        $this->logger()->log(LogLevel::SUCCESS, \dt('Succeeded in removing "@machine_name" from system.schema key/value storage.', [
          '@machine_name' => $machine_name,
        ]));
      }
      else {
        $this->logger()->log(LogLevel::ERROR, \dt('Aborted remove of "@machine_name" from system.schema key/value storage, because specified machine name is already installed.', [
          '@machine_name' => $machine_name,
        ]));
      }
    }
    else {
      $this->logger()->log(LogLevel::WARNING, \dt('Specified machine name "@machine_name" is not exists to the system.schema key/value storage. (* This message is displayed if it may have already been deleted.)', [
        '@machine_name' => $machine_name,
      ]));
    }
  }

}
